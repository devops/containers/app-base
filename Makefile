SHELL = /bin/bash

base_image ?= gitlab-registry.oit.duke.edu/devops/containers/debian-buster:main
build_tag ?= app-base

.PHONY: build
build:
	docker pull $(base_image)
	DOCKER_BUILDKIT=1 docker build -t $(build_tag) \
		--build-arg base_image=$(base_image) \
		- \
		< ./Dockerfile

.PHONY: clean
clean:
	echo 'no-op'

.PHONY: test
test:
	docker run --rm $(build_tag) /bin/sh -c 'echo "Hello, world!"'
