ARG base_image
FROM ${base_image}

ARG app_root=/opt/app-root
ARG app_user_uid=1001
ARG app_user_gid=0
ARG app_user_name=app-user
ARG app_user_shell=/usr/sbin/nologin

ENV APP_ROOT=$app_root \
	APP_USER_UID=$app_user_uid \
	APP_USER_GID=$app_user_gid \
	APP_USER_NAME=$app_user_name

RUN useradd \
	--uid $APP_USER_UID \
	--gid $APP_USER_GID \
	--no-user-group \
	--create-home \
	--shell $app_user_shell \
	$APP_USER_NAME
